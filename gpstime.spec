Name:           gpstime
Version:        0.8.2
Release:        1%{?dist}
Summary:        LIGO GPS time libraries

License:        GPLv3+
URL:            http://www.ligo.org
Source0:        %pypi_source

BuildArch:      noarch
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-setuptools_scm
BuildRequires:  python%{python3_pkgversion}-devel

%description
GPS time libraries for LIGO automation software

%package -n     python%{python3_pkgversion}-%{name}
Summary:        LIGO GPS time libraries
Requires:	python%{python3_pkgversion}-dateutil
Requires:	python%{python3_pkgversion}-requests
%if 0%{?rhel} == 7
Requires:	python3-appdirs
%endif
%{?python_provide:%python_provide python%{python3_pkgversion}-%{name}}
%description -n python%{python3_pkgversion}-%{name}
GPS time libraries for LIGO automation software


%prep
%setup -q


%build
%py3_build


%install
rm -rf $RPM_BUILD_ROOT

%py3_install

%files -n python%{python3_pkgversion}-%{name}
%{python3_sitelib}/*
%{_bindir}/gpstime

%changelog
* Thu Jul 18 2024 Adam Mercer <adam.mercer@ligo.org> - 0.8.2-1
- Update to 0.8.2

* Mon Dec 25 2023 Michael Thomas <michael.thomas@LIGO.ORG> - 0.8.1-1
- Update to 0.8.1

* Fri Aug 26 2022 Michael Thomas <michael.thomas@LIGO.ORG> - 0.6.2-2
- Dependency updates

* Thu Aug 25 2022 Michael Thomas <michael.thomas@LIGO.ORG> - 0.6.2-1
- Update to 0.6.2
- Drop python2 support

* Fri Dec 6 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 0.4.4-1
- Update to 0.4.4
- Drop python3.4 support
- Update CLI to use python3

* Thu Mar 28 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 0.3.2-1
- Updates to 0.3.2
- Add missing dependencies

* Thu Nov 29 2018 Michael Thomas <michael.thomas@LIGO.ORG> - 0.3.1-7
- Add python3.6 support

* Tue Nov 6 2018 Michael Thomas <mthomas@ligo-la.caltech.edu> 0.3.1-6
- Remove duplicate installation files

* Tue Nov 6 2018 Michael Thomas <mthomas@ligo-la.caltech.edu> 0.3.1-5
- Fix broken obsoletes/provides

* Fri Oct 26 2018 Michael Thomas <mthomas@ligo-la.caltech.edu> 0.3.1-4
- Update to support python2 and python3

* Thu Jan 25 2018 Michael Thomas <mthomas@ligo-la.caltech.edu> 0.3.1-3
- Change run interval in the timer unit file.

* Wed Jan 24 2018 Michael Thomas <mthomas@ligo-la.caltech.edu> 0.3.1-2
- Ghost the leap second file so that it gets deleted along with the package

* Wed Oct 12 2016 Michael Thomas <mthomas@ligo-la.caltech.edu> 0.1.2-1
- Initial package
